# ocisnap
ocisnap is a simple utility for creating snapshots of OCI file systems (file storage, not block storage) based on the presence of file system tags.

[https://docs.oracle.com/en-us/iaas/Content/File/Concepts/filestorageoverview.htm](https://docs.oracle.com/en-us/iaas/Content/File/Concepts/filestorageoverview.htm)

## Example usage
```
Usage: ocisnap [ -d ] [ -r ] [ -t <timeinseconds> ] [ -c <compartmentocid> ] -T KEY:VALUE -N KEY:VALUE

# Snap all volumes in all compartments tagged with Backup:True, tag snapshots with CreatedByService:go-ocisnap
$ ocisnap -T Backup:TRUE -N CreatedByService:go-ocisnap -r

# Snap all volumes in a single compartment
$ ocisnap -T Backup:TRUE -N CreatedbyService:go-ocisnap -c ocid1.tenancy.oc1..aaaaaaaa3zyow6hnryee7taiaxdegeirmlr5ujkbfkdsfrzknqqzsnu57zdq

# Snap all volumes in a given compartment, include all child compartments
$ ocisnap -T Backup:TRUE -N CreatedbyService:go-ocisnap -r -c ocid1.tenancy.oc1..aaaaaaaa3zyow6hnryee7taiaxdegeirmlr5ujkbfkdsfrzknqqzsnu57zdq

# Recursively snap all volumes in a compartment tagged with Backup:Gold and remove all old snaps over one week old
$ ocisnap -T Backup:Gold -N CreatedbyService:go-ocisnap-gold -t 604800 -r -c ocid1.tenancy.oc1..aaaaaaaa3zyow6hnryee7taiaxdegeirmlr5ujkbfkdsfrzknqqzsnu57zdq

# Recursively snap all volumes in a compartment tagged with Backup:Silver and remove all old snaps over 3 days old
$ ocisnap -T Backup:Silver -N CreatedbyService:go-ocisnap-silver -t 259200 -r -c ocid1.tenancy.oc1..aaaaaaaa3zyow6hnryee7taiaxdegeirmlr5ujkbfkdsfrzknqqzsnu57zdq

# Note: Tags
# 1. Tags are specified KEY:VALUE
# 2. -N sets the service tag of the snapshot. This is matched when deleting snapshots

# Note: Compartment selection
# 1. If compartmentocid is NOT supplied, all compartments of the authenticated user are used.
# 2. If compartmentocid is supplied without -r, only volumes in that compartment are examined for tags.
# 3. If compartmentocid is supplied with -r, all volumes in that compartment and all children are examined for tags.
```

## Tags
In order for a filesystem to be targeted for snap, it must be tagged accordingly. ```ocisnap``` allows the selection tag to be specified using a cli parameter```(-T)```.
Tags are specified using a simple colon separated string in the form ```TAG_KEY:TAG_VALUE```. Tags should be added as **free-form** tags to the volumes that require snapshots. 

All created snapshots are tagged to allow later identification, the identity tag is specified using the ```-N``` cli parameter.

## Snap Deletion
```ocisnap``` allows for automatic deletion of old snap shots. This feature is enabled using the ```-t``` flag. All snaps that exceed this age and are tagged with the specified snap tag (```-N```) will be deleted. The combination of ```-t```, ```-N``` and ```-T``` allows for multiple instances of ```ocisnap``` to be used to enforce different snap retention policies. Consider the following example.

- All File systems wth the free-form tags ```Backup:Gold``` should be snapped with a snapshot retention of 1 week.
- All File systems wth the free-form tags ```Backup:Silver``` should be snapped with a snapshot retention of 3 days.

Multiple instances of ```ocisnap``` could be run as follows.
```
# Recursively snap all volumes tagged with Backup:Gold and remove all old snaps over one week old
$ ocisnap -T Backup:Gold -N CreatedbyService:go-ocisnap-gold -t 604800 -r 

# Recursively snap all volumes tagged with Backup:Silver and remove all old snaps over 3 days old
$ ocisnap -T Backup:Silver -N CreatedbyService:go-ocisnap-silver -t 259200 -r 
```

## Requirements 
```ocisnap``` has been tested with ```go``` version ```1.16```

## Building
The following example build instructions pertain to a Unix based operating system.
```
# Install go on your platform

# Ensure GOPATH is set
$ mkdir ~/go ; export GOPATH=~/go

# Clone the repo
$ git clone <repoUrl>

# Build
$ cd ociman
$ go get ./...
$ go build

```
Should result in a single binary called ```ocisnap``` to be created in the working directory.
Run ```ocisnap --help``` for full usage information.

## Notes
### Authentication
```ocisnap``` uses the supported authentication mechanisms of the Oracle OCI Go SDK. Please see the following links for details.
- [https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/gosdk.htm](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/gosdk.htm)
- [https://docs.oracle.com/en-us/iaas/Content/API/Concepts/sdkconfig.htm#SDK_and_CLI_Configuration_File](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/sdkconfig.htm#SDK_and_CLI_Configuration_File)

### Snapshots
A note about snapshots. ```ocisnap``` behaves similar to an OCI gui operator in the way that snapshots are created. Snaps are simply requested, no mechanism is implemented to wait for and report the results of the snap request. We trust the OCI API to "do the right thing" regarding the snap request, however, it would be possible to make ```ocisnap``` monitor the result of the requests and wait until completion or failure. This feature may be considered in future releases.

### Compartment selection
I decided to go with requiring an ocid for compartment selection due to the fact this is guaranteed to be unique. If you're targeting the volumes of a particular compartment, you need to know some identifying information and the OCI console and cli provides convenient mechanisms for getting the ocid for a given compartment. 

## TODO
- Tests Tests Tests and more Tests
- Code re-factor
- Functional tests
- CI/CD Pipeline
- Support for OCI function deployment
- Docker image


