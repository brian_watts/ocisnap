package main

import (
	"flag"
	"fmt"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/brian_watts/ocisnap/logic"
	"os"
	"strings"
	// "github.com/oracle/oci-go-sdk/v47/common"
	// "github.com/oracle/oci-go-sdk/v47/example/helpers"
	// "github.com/oracle/oci-go-sdk/v47/identity"
	// "github.com/oracle/oci-go-sdk/v47/filestorage"
)

func validateTags(tag string) (string, string) {
	tagParts := strings.Split(tag, ":")
	if len(tagParts) != 2 || (tagParts[0] == "" || tagParts[1] == "") {
		fmt.Println("error, invalid tag parameter")
		os.Exit(1)
	}
	return tagParts[0], tagParts[1]
}

func main() {

	bnf := `Usage: ocisnap [ -d ] [ -r ] [ -t <timeinseconds> ] [ -c <compartmentocid> ] -T KEY:VALUE -N KEY:VALUE

ocisnap is a simple utility for creating snapshots of OCI volumes based on the presence of tags. 
Examples:

     # Snap all volumes in all compartments tagged with Backup:True, tag snapshots with CreatedByService:go-ocisnap
     $ ocisnap -T Backup:TRUE -N CreatedByService:go-ocisnap -r 

     # Snap all volumes in a single compartment
     $ ocisnap -T Backup:TRUE -N CreatedbyService:go-ocisnap -c ocid1.tenancy.oc1..aaaaaaaa3zyow6hnryee7taiaxdegeirmlr5ujkbfkdsfrzknqqzsnu57zdq

     # Snap all volumes in a given compartment, include all child compartments
     $ ocisnap -T Backup:TRUE -N CreatedbyService:go-ocisnap -r -c ocid1.tenancy.oc1..aaaaaaaa3zyow6hnryee7taiaxdegeirmlr5ujkbfkdsfrzknqqzsnu57zdq

     # Recursively snap all volumes in a compartment tagged with Backup:Gold and remove all old snaps over one week old
     $ ocisnap -T Backup:Gold -N CreatedbyService:go-ocisnap-gold -t 604800 -r -c ocid1.tenancy.oc1..aaaaaaaa3zyow6hnryee7taiaxdegeirmlr5ujkbfkdsfrzknqqzsnu57zdq

     # Recursively snap all volumes in a compartment tagged with Backup:Silver and remove all old snaps over 3 days old
     $ ocisnap -T Backup:Silver -N CreatedbyService:go-ocisnap-silver -t 259200 -r -c ocid1.tenancy.oc1..aaaaaaaa3zyow6hnryee7taiaxdegeirmlr5ujkbfkdsfrzknqqzsnu57zdq

     # Note: Tags
     # 1. Tags are specified KEY:VALUE
     # 2. -N sets the service tag of the snapshot. This is matched when deleting snapshots

     # Note: Compartment selection
     # 1. If compartmentocid is NOT supplied, all compartments of the authenticated user are used.
     # 2. If compartmentocid is supplied without -r, only volumes in that compartment are examined for tags.
     # 3. If compartmentocid is supplied with -r, all volumes in that compartment and all children are examined for tags.

Options:`

	// CLI procesing
	d := flag.Bool("d", false, "Enable debugging")
	r := flag.Bool("r", false, "Recurse all child compartments ")
	c := flag.String("c", "", "The ocid of the starting compartment")
	T := flag.String("T", "", "The free-form volume tag used to select volume for snapshot")
	N := flag.String("N", "", "The free-form tag to use when creating snapshots")
	t := flag.Int64("t", 0, "The maximum age of a snap shot in seconds, must be > 0. Forces removal of old snapshots")
	flag.Usage = func() {
		umsg := bnf
		fmt.Fprintf(flag.CommandLine.Output(), umsg+"\n\n")
		flag.PrintDefaults()
	}
	flag.Parse()

	// Validate CLI params
	// Ensure tags can be split into two fields
	volTagKey, volTagVal := validateTags(*T)
	snapTagKey, snapTagVal := validateTags(*N)

	// Setup Logging
	var logger hclog.Logger
	if *d {
		logger = hclog.New(&hclog.LoggerOptions{Name: "ocisnap", Level: hclog.LevelFromString("DEBUG")})
		//logger = hclog.New(&hclog.LoggerOptions{ Name: "ocisnap", Level: hclog.LevelFromString("DEBUG"), JSONFormat: true})
	} else {
		logger = hclog.Default()
	}

	sm := logic.NewSnapManager(*r, *c, *t, volTagKey, volTagVal, snapTagKey, snapTagVal, logger)
	sm.Process()
}
