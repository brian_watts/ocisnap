module gitlab.com/brian_watts/ocisnap

go 1.16

require (
	github.com/hashicorp/go-hclog v0.16.2
	github.com/oracle/oci-go-sdk/v47 v47.1.0
)
