package utils

type FlagList []string

func (i *FlagList) String() string {
	return ""
}

func (i *FlagList) Set(value string) error {
	*i = append(*i, value)
	return nil
}

type StrSlice []string

func (l *StrSlice) Pop() string {
	length := len(*l)
	if length == 0 {
		return ""
	}
	lastEle := (*l)[length-1]
	*l = (*l)[:length-1]

	return lastEle
}
