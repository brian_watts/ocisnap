package logic

import (
	//"fmt"
	"strconv"
	"time"
	// "os"
	"github.com/hashicorp/go-hclog"
	"github.com/oracle/oci-go-sdk/v47/filestorage"
	//	"github.com/oracle/oci-go-sdk/v47/common"
	"gitlab.com/brian_watts/ocisnap/clients"
	"gitlab.com/brian_watts/ocisnap/utils"
)

type SnapManager struct {
	idclient   *clients.IdentityClient
	fsclient   *clients.FileStorageClient
	avdList    *utils.StrSlice
	rootCID    string
	recurse    bool
	cid        string
	snapMaxAge int64
	volTagKey  string
	volTagVal  string
	snapTagKey string
	snapTagVal string
	log        hclog.Logger
}

func NewSnapManager(recurse bool, cid string, snapMaxAge int64,
	volTagKey string, volTagVal string, snapTagKey string, snapTagVal string, log hclog.Logger) *SnapManager {
	fsc := clients.NewFileStorageClient(log)
	idc := clients.NewIdentityClient(log)
	rootCID := clients.GetRootCompartmentId()
	avdList := clients.AvailabilityDomainsToStrSlice(idc.GetAvailabilityDomains(rootCID))

	sm := &SnapManager{idc, fsc, avdList, rootCID, recurse,
		cid, snapMaxAge, volTagKey, volTagVal, snapTagKey, snapTagVal, log}

	return sm
}

func (sm *SnapManager) setFilesystems(compList *utils.StrSlice) *utils.StrSlice {
	var allFs []filestorage.FileSystemSummary
	for _, cmpId := range *compList {
		for _, avdId := range *sm.avdList {
			allFs = append(allFs, *sm.fsclient.GetFileSystems(cmpId, avdId)...)
		}
	}

	return clients.FileSystemsToStrSlice(&allFs)
}

func (sm *SnapManager) setFilesystemsWithTags(compList *utils.StrSlice, tagKey string, tagValue string) *utils.StrSlice {
	var allFs []filestorage.FileSystemSummary
	for _, cmpId := range *compList {
		for _, avdId := range *sm.avdList {
			allFs = append(allFs, *sm.fsclient.GetFileSystemsWithTags(cmpId, avdId, tagKey, tagValue)...)
		}
	}

	return clients.FileSystemsToStrSlice(&allFs)
}

func (sm *SnapManager) setCompartments(cid string, recurse bool) *utils.StrSlice {
	var compartmentList *utils.StrSlice
	var compartments utils.StrSlice
	if recurse {
		sm.log.Info("Recursion enabled", "compartmentID", cid)
		allCmpts := sm.idclient.GetCompartments(sm.rootCID)
		compartmentList = clients.ReduceCompartments(cid, allCmpts)
	} else {
		sm.log.Info("Recursion disabled", "compartmentID", cid)
		cl := sm.idclient.GetCompartment(cid)
		compartments := append(compartments, *cl.Id)
		compartmentList = &compartments
	}

	return compartmentList
}

func (sm *SnapManager) removeSnaps(fsId string) {
	var snapList *[]filestorage.SnapshotSummary

	sm.log.Debug("Getting snaps", "fsId", fsId)
	snapList = sm.fsclient.GetSnapShots(fsId, sm.snapTagKey, sm.snapTagVal)
	for _, snap := range *snapList {
		if (time.Now().Unix() - snap.TimeCreated.Time.Unix()) >= sm.snapMaxAge {
			sm.log.Info("Removing snap", "snapId", *snap.Id, "fsId", fsId)
			sm.fsclient.DeleteSnapshot(*snap.Id)
		}
	}
}

func (sm *SnapManager) removeOldSnapshots(fsList *utils.StrSlice) {
	sm.log.Info("Removing old snapshots")
	for _, fsId := range *fsList {
		sm.log.Info("Checking filesystem for old snapshots", "fsId", fsId)
		sm.removeSnaps(fsId)
	}
}

func (sm *SnapManager) Process() {
	sm.log.Info("Snapshot processing started")
	var cmpList *utils.StrSlice
	var fsList *utils.StrSlice

	// Determine the compartment list
	// If cid is supplied, snap all tagged volumes, including children if recurse is set
	if sm.cid != "" {
		sm.log.Info("Creating snapshots", "compartmentId", sm.cid)
		cmpList = sm.setCompartments(sm.cid, sm.recurse)
		// Otherwise look for filesystems in all compartments
	} else {
		sm.log.Info("Creating snapshots for all compartments")
		cmpList = clients.CompartmentsToStrSlice(sm.idclient.GetCompartments(sm.rootCID))
	}
	// Create a filesystem list for the determined compartments
	if sm.volTagKey != "" && sm.volTagVal != "" {
		sm.log.Info("Selecting filesystems that match tag", "tagKey", sm.volTagKey, "tagValue", sm.volTagVal)
		fsList = sm.setFilesystemsWithTags(cmpList, sm.volTagKey, sm.volTagVal)
	} else {
		sm.log.Info("Selecting all filesystems")
		fsList = sm.setFilesystems(cmpList)
	}
	// Snap the filesystems
	for _, fsId := range *fsList {
		sm.log.Info("Creating snapshot", "fsId", fsId)
		now := time.Now()
		secs := now.Unix()
		sm.fsclient.CreateSnapshot(fsId, "ocisnap_"+strconv.FormatInt(int64(secs), 10), sm.snapTagKey, sm.snapTagVal)
	}
	// Remove old snaps
	if sm.snapMaxAge > 0 {
		sm.removeOldSnapshots(fsList)
	}
}
