package clients

import (
	"context"

	"github.com/hashicorp/go-hclog"
	"github.com/oracle/oci-go-sdk/v47/common"
	"github.com/oracle/oci-go-sdk/v47/example/helpers"
	"github.com/oracle/oci-go-sdk/v47/identity"
	"gitlab.com/brian_watts/ocisnap/utils"
)

type IdentityClient struct {
	log    hclog.Logger
	client identity.IdentityClient
}

func NewIdentityClient(log hclog.Logger) *IdentityClient {
	id_client, err := identity.NewIdentityClientWithConfigurationProvider(common.DefaultConfigProvider())
	helpers.FatalIfError(err)
	idc := &IdentityClient{log, id_client}

	return idc
}

func (idc *IdentityClient) GetCompartment(compartmentId string) identity.Compartment {
	request := identity.GetCompartmentRequest{
		CompartmentId:   &compartmentId,
		RequestMetadata: helpers.GetRequestMetadataWithDefaultRetryPolicy(),
	}

	ctx := context.Background()
	resp, err := idc.client.GetCompartment(ctx, request)
	helpers.FatalIfError(err)

	return resp.Compartment
}

func (idc *IdentityClient) GetCompartments(tenancyID string) *[]identity.Compartment {
	req := identity.ListCompartmentsRequest{LifecycleState: identity.CompartmentLifecycleStateActive,
		AccessLevel:            identity.ListCompartmentsAccessLevelAny,
		CompartmentId:          common.String(tenancyID),
		CompartmentIdInSubtree: common.Bool(true),
		SortOrder:              identity.ListCompartmentsSortOrderDesc}

	idc.log.Debug("Getting all compartments", "tenantID", tenancyID)
	resp, err := idc.client.ListCompartments(context.Background(), req)
	helpers.FatalIfError(err)

	return &resp.Items
}

func (idc *IdentityClient) GetAvailabilityDomains(compId string) *[]identity.AvailabilityDomain {
	req := identity.ListAvailabilityDomainsRequest{CompartmentId: common.String(compId)}

	idc.log.Debug("Getting availability domains", "compartmentID", compId)
	resp, err := idc.client.ListAvailabilityDomains(context.Background(), req)
	helpers.FatalIfError(err)

	return &resp.Items
}

func AvailabilityDomainsToStrSlice(avds *[]identity.AvailabilityDomain) *utils.StrSlice {
	var avdList utils.StrSlice
	for _, avd := range *avds {
		avdList = append(avdList, *avd.Name)
	}

	return &avdList
}

func CompartmentsToStrSlice(compartments *[]identity.Compartment) *utils.StrSlice {
	var cmpList utils.StrSlice
	for _, cmp := range *compartments {
		cmpList = append(cmpList, *cmp.Id)
	}

	return &cmpList
}

func ReduceCompartments(parent string, compartmentList *[]identity.Compartment) *utils.StrSlice {
	var r utils.StrSlice
	var tocheck utils.StrSlice
	var pid string

	r = append(r, parent)
	tocheck = append(tocheck, parent)
	for {
		pid = tocheck.Pop()
		if pid == "" {
			break
		}
		for _, comp := range *compartmentList {
			if *comp.CompartmentId == pid {
				tocheck = append(tocheck, *comp.Id)
				r = append(r, *comp.Id)
			}
		}
	}

	return &r
}

func GetRootCompartmentId() string {
	tenancyId, err := common.DefaultConfigProvider().TenancyOCID()
	helpers.FatalIfError(err)

	return tenancyId
}
