package clients

import (
	"context"
	"net/http"

	"github.com/hashicorp/go-hclog"
	"github.com/oracle/oci-go-sdk/v47/common"
	"github.com/oracle/oci-go-sdk/v47/example/helpers"
	"github.com/oracle/oci-go-sdk/v47/filestorage"
	"gitlab.com/brian_watts/ocisnap/utils"
)

type FileStorageClient struct {
	log    hclog.Logger
	client filestorage.FileStorageClient
}

func NewFileStorageClient(log hclog.Logger) *FileStorageClient {
	fsclient, fscerr := filestorage.NewFileStorageClientWithConfigurationProvider(common.DefaultConfigProvider())
	helpers.FatalIfError(fscerr)
	fsc := &FileStorageClient{log, fsclient}

	return fsc
}

func (fsc *FileStorageClient) GetFileSystems(compId string, avDomId string) *[]filestorage.FileSystemSummary {
	fsc.log.Debug("Getting all file systems", "compartment", compId, "availability_domain", avDomId)
	req := filestorage.ListFileSystemsRequest{AvailabilityDomain: common.String(avDomId),
		CompartmentId: common.String(compId)}
	resp, err := fsc.client.ListFileSystems(context.Background(), req)
	helpers.FatalIfError(err)

	return &resp.Items
}

// FIXME: Consider matching multiple selection tags
// func (fsc *FileStorageClient) GetFileSystemsWithTags(compId string, avDomId string, tags map[string]string) *[]filestorage.FileSystemSummary {
func (fsc *FileStorageClient) GetFileSystemsWithTags(compId string, avDomId string, tagKey string, tagValue string) *[]filestorage.FileSystemSummary {
	var request filestorage.ListFileSystemsRequest
	var fsList []filestorage.FileSystemSummary

	request.AvailabilityDomain = common.String(avDomId)
	request.CompartmentId = common.String(compId)

	fsc.log.Debug("Getting all file systems that match tags", "compartment", compId, "availability_domain", avDomId)
	resp, err := fsc.client.ListFileSystems(context.Background(), request)
	helpers.FatalIfError(err)

	// Filter filesystems on tags
	for _, fs := range resp.Items {
		for key, value := range fs.FreeformTags {
			if key == tagKey && value == tagValue {
				fsList = append(fsList, fs)
			}
		}
	}

	return &fsList
}

func (fsc *FileStorageClient) GetSnapAllShots(fsId string) *[]filestorage.SnapshotSummary {
	fsc.log.Debug("Getting all snapshots", "filesystem", fsId)
	req := filestorage.ListSnapshotsRequest{FileSystemId: common.String(fsId)}

	resp, err := fsc.client.ListSnapshots(context.Background(), req)
	helpers.FatalIfError(err)

	return &resp.Items
}

func (fsc *FileStorageClient) GetSnapShots(fsId string, snapTagKey string, snapTagVal string) *[]filestorage.SnapshotSummary {
	var snapList []filestorage.SnapshotSummary

	fsc.log.Debug("Getting all snapshots that match tag", "filesystem", fsId, "tagKey", snapTagKey, "tagValue", snapTagVal)
	req := filestorage.ListSnapshotsRequest{FileSystemId: common.String(fsId)}

	resp, err := fsc.client.ListSnapshots(context.Background(), req)
	helpers.FatalIfError(err)

	// Only return the snaps we created
	for _, snap := range resp.Items {
		for key, value := range snap.FreeformTags {
			if key == snapTagKey && value == snapTagVal {
				snapList = append(snapList, snap)
			}
		}
	}

	return &resp.Items
}

func (fsc *FileStorageClient) CreateSnapshot(fsId string, snapName string, snapTagKey string, snapTagVal string) *http.Response {
	var details filestorage.CreateSnapshotDetails

	details.FileSystemId = common.String(fsId)
	details.Name = common.String(snapName)
	tags := make(map[string]string)
	tags[snapTagKey] = snapTagVal
	details.FreeformTags = tags

	fsc.log.Debug("Creating snapshot", "filesystem", fsId, "name", snapName)
	req := filestorage.CreateSnapshotRequest{CreateSnapshotDetails: details}
	resp, err := fsc.client.CreateSnapshot(context.Background(), req)
	helpers.FatalIfError(err)

	return resp.RawResponse
}

func (fsc *FileStorageClient) DeleteSnapshot(snapId string) *http.Response {
	fsc.log.Debug("Deleting snapshot", "Id", snapId)
	req := filestorage.DeleteSnapshotRequest{SnapshotId: common.String(snapId)}

	resp, err := fsc.client.DeleteSnapshot(context.Background(), req)
	helpers.FatalIfError(err)

	return resp.RawResponse
}

func FileSystemsToStrSlice(fsl *[]filestorage.FileSystemSummary) *utils.StrSlice {
	var fsList utils.StrSlice
	for _, fs := range *fsl {
		fsList = append(fsList, *fs.Id)
	}

	return &fsList
}
